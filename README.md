# TG HommaBot

## Installation and running

    poetry install
    poetry run python hommaexceli_py/main.py

## Author

Jan Tuomi \<jans.tuomi@gmail.com\>
