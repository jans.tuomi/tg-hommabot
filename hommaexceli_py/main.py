import logging
from dotenv import load_dotenv, dotenv_values

load_dotenv()

from telegram_client import run_telegram

logging.basicConfig(
    level=logging.INFO, format="%(asctime)s | %(name)s | %(levelname)s | %(message)s"
)


def main():
    logging.info("Starting hommaexceli_py...")
    logging.info("Using environment:")

    env = dotenv_values()
    for env_key in env.keys():
        logging.info(f"{env_key}={env[env_key]}")

    run_telegram()


if __name__ == "__main__":
    main()