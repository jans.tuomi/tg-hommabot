from dataclasses import dataclass
import datetime


@dataclass
class SheetsRow:
    interval: str
    name: str
    last_done: str

    def __str__(self):
        return f"{self.interval}, {self.name}, {self.last_done}"


@dataclass
class ProcessedRow:
    name: str
    next_datestamp: str
    next_date: datetime.datetime
    last_datestamp: str
    last_date: datetime.datetime
    interval: str
